# restvaccine-RegionH
## About
Console app to automate signup process for surplus vaccines in Region Hovedstaden.
Hastily put together. Use responsibly.

Inspired by [KJerslev's restvaccine](https://github.com/KJerslev/restvaccine/blob/main/restvaccine.py).
## Vaccination spots
Paste any of the following locations in the JSON file under 'location'

### Øksnehallen, Halmtorvet 11, København V:
`//label[@for='ch_50088941-106780586']`
### Ballerup, Baltorpvej 18
`//label[@for='ch_50088941-106780581']`
### Bella Center, Ørestad Boulevard/Martha Christensens Vej, København S
`//label[@for='ch_50088941-106780582']` 
### Bornholm, Ullasvej 39 C, Rønne
`//label[@for='ch_50088941-106780583']` 
### Hillerød, Østergade 8
`//label[@for='ch_50088941-106780584']`
### Ishøj, Vejledalen 17
`//label[@for='ch_50088941-106780585']`
### Snekkerstenhallen, Agnetevej 1
`//label[@for='ch_50088941-132274726']`
### Vaccinationscenter Birkerød, Søndervangen 44, 3460 Birkerød
`//label[@for='ch_50088941-168745368']`
### Frederikssund Hospital, Frederikssundsvej 30 (kun opskrivning  torsdag)
`//label[@for='ch_50088941-187278225']`