﻿using System;
using System.IO;
using System.Threading;
using System.Text.Json;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using WebDriverManager;
using WebDriverManager.DriverConfigs.Impl;

namespace EasyVac
{
    class Program
    {
        private static IWebDriver _chromeDriver;
        static void Main()
        {
            CreateWebDriver();
            SignUp();
        }

        private static void CreateWebDriver()
        {
            DriverManager webDriverManager = new DriverManager();
            webDriverManager.SetUpDriver(new ChromeConfig());
            _chromeDriver = new ChromeDriver(Environment.CurrentDirectory);
        }

        private static void SignUp()
        {
            string currentDirectory = Directory.GetCurrentDirectory();
            string jsonString = File.ReadAllText(currentDirectory + @"\PersonInfo.json");
            Person person = JsonSerializer.Deserialize<Person>(jsonString);
            var nextButton = "input.next-button";

            var link =
                @"https://www.survey-xact.dk/LinkCollector?key=JPH8ZGHNL21N";
            
            _chromeDriver.Navigate().GoToUrl(link);
            Thread.Sleep(500);
            
            _chromeDriver.FindElement(By.ClassName("next-button")).Click(); // start survey
            Thread.Sleep(500);

            if (person != null && !string.IsNullOrEmpty(person.Name))
            {
                _chromeDriver.FindElement(By.XPath("//label[@for='ch_101770541-256179674']")).Click(); // answer no to having received first shot of vaccine
                _chromeDriver.FindElement(By.CssSelector(nextButton)).Click(); // click next
                Thread.Sleep(500);

                _chromeDriver.FindElement(By.Id("t50100775")).SendKeys(person.Name); // fill in yourName
                _chromeDriver.FindElement(By.CssSelector(nextButton)).Click(); // click next
                Thread.Sleep(500);

                _chromeDriver.FindElement(By.Id("n35965768")).SendKeys(person.Age); // fill in age
                _chromeDriver.FindElement(By.CssSelector(nextButton)).Click(); // click next
                Thread.Sleep(500);

                _chromeDriver.FindElement(By.Id("t50088645")).SendKeys(person.Address); // fill in address
                _chromeDriver.FindElement(By.CssSelector(nextButton)).Click(); // click next
                Thread.Sleep(500);

                _chromeDriver.FindElement(By.Id("t50088674")).SendKeys(person.City); // fill in zipcode and city
                _chromeDriver.FindElement(By.CssSelector(nextButton)).Click(); // click next
                Thread.Sleep(500);

                _chromeDriver.FindElement(By.Id("n50088775")).SendKeys(person.Phone); // fill in phone number
                _chromeDriver.FindElement(By.CssSelector(nextButton)).Click(); // click next
                Thread.Sleep(500);

                _chromeDriver.FindElement(By.XPath(person.Location)).Click(); // select vaccine spot
                _chromeDriver.FindElement(By.CssSelector(nextButton)).Click(); // click next
                Thread.Sleep(500);

                _chromeDriver.FindElement(By.CssSelector(nextButton)).Click(); // go to submission
                Thread.Sleep(500);
                
                _chromeDriver.FindElement(By.CssSelector(nextButton)).Click(); // submit the form
                Thread.Sleep(500);
                
                _chromeDriver.Quit(); // close Chrome
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Success! Signed up for waiting list.");
                Thread.Sleep(3000);
            }
            else
            {
                _chromeDriver.Quit();
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Missing JSON data - please fill out! Press 'Enter' to close.");
                Console.Read();
            }

        }
    }
}